# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-inflect
pkgver=5.0.1
pkgrel=0
pkgdesc="Correctly generate plurals, singular nouns, ordinals, indefinite articles; convert numbers to words"
url="https://github.com/jazzband/inflect"
arch="noarch !mips !mips64 !s390x" # py3-pytest-black->black
license="MIT"
depends="python3"
makedepends="
	py3-setuptools
	py3-setuptools_scm
	py3-toml
	"
checkdepends="
	py3-pytest
	py3-pytest-black
	py3-pytest-cov
	py3-pytest-flake8
	"
source="https://pypi.python.org/packages/source/i/inflect/inflect-$pkgver.tar.gz"
builddir="$srcdir/inflect-$pkgver"

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD/build/lib" pytest --ignore setup.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="edd5de19eaf5eae50d588c519d08f6b0448db5623f761863b06732f75473e9eedc035d6d7b1037469e97c876de3ff718db8ebb58ae827c23308ca90660f484fd  inflect-5.0.1.tar.gz"
